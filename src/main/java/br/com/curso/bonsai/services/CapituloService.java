package br.com.curso.bonsai.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.Capitulo;
import br.com.curso.bonsai.entity.dto.CapituloDTO;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.repositories.CapituloRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class CapituloService {

	@Autowired
	private CapituloRepository repo;
	@Autowired
	private CursoService cursoService;

	public Capitulo find(Integer id) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		Optional<Capitulo> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Capitulo.class.getName()));
	}
	
	public Capitulo findByNomeCurso(String nomeCurso) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN)) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		Optional<Capitulo> obj = Optional.of(repo.findByCurso(nomeCurso));
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Curso: " + nomeCurso + ", Tipo: " + Capitulo.class.getName()));
	}
	
	public Page<Capitulo> search(String nome, Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.search(nome, pageRequest);
	}

	@Transactional
	public Capitulo insert(Capitulo obj) {
		obj.setId(null);
		obj.setCurso(cursoService.find(obj.getCurso().getId()));
		obj = repo.save(obj);
		return obj;
	}
	
	public Capitulo update(Capitulo obj) {
		Capitulo newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}
	
	public List<Capitulo> findAll() {
		return repo.findAll();
	}
	
	public Page<Capitulo> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public Capitulo fromDTO(CapituloDTO objDto) {
		return new Capitulo(objDto.getId(), objDto.getTitulo(), objDto.getDescricao(), objDto.getPathImagem(), null);
	}
	
	public CapituloDTO fromEntityToDTO(Capitulo obj) {
		return new CapituloDTO(obj);
	}
	
	private void updateDate(Capitulo newObj, Capitulo obj) {
		newObj.setTitulo(obj.getTitulo());
		newObj.setDescricao(obj.getDescricao());
		newObj.setPathImagem(obj.getPathImagem());
	}

}

package br.com.curso.bonsai.services;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.curso.bonsai.entity.Categoria;
import br.com.curso.bonsai.entity.Curso;
import br.com.curso.bonsai.entity.dto.CursoDTO;
import br.com.curso.bonsai.repositories.CategoriaRepository;
import br.com.curso.bonsai.repositories.CursoRepository;
import br.com.curso.bonsai.services.aws.S3Service;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class CursoService {

	@Autowired
	private CursoRepository repo;
	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private S3Service s3Service;
	@Autowired
	private CategoriaService categoriaService;

	public Curso find(Integer id) throws ObjectNotFoundException {
		Optional<Curso> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Curso.class.getName()));
	}
	
	@Transactional
	public Curso insert(Curso obj) {
		obj.setId(null);
		obj.setCategoria(categoriaService.find(obj.getCategoria().getId()));
		obj = repo.save(obj);
		return obj;
	}
	
	public Curso update(Curso obj) {
		Curso newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}
	
	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}
	
	public List<Curso> findAll() {
		return repo.findAll();
	}
	
	public Page<Curso> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public Curso fromDTO(CursoDTO objDto) {
		return new Curso(objDto.getId(), objDto.getNome(), objDto.getDescricao(), objDto.getPreco(), objDto.getPathImagem(), null);
	}
	
	public CursoDTO fromEntityToDTO(Curso obj) {
		return new CursoDTO(obj);
	}
	
	private void updateDate(Curso newObj, Curso obj) {
		newObj.setNome(obj.getNome());
		newObj.setDescricao(obj.getDescricao());
		newObj.setPreco(obj.getPreco());
	}
	
	public Page<Curso> search(String nome, Integer id, Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Optional<Categoria> categoria = categoriaRepository.findById(id);
		Categoria cat = new Categoria();
		if(categoria.isPresent()) {
			cat = categoria.get();
		}
		return repo.search(nome, cat.getId(), pageRequest);
	}
	
	public URI uploadPicture(MultipartFile multipartFile) {
		return s3Service.uploadFile(multipartFile);
	}

}

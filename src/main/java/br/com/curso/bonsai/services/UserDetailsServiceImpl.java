package br.com.curso.bonsai.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private AlunoRepository repo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Aluno aluno = repo.findByEmail(email);
		if(aluno == null) {
			throw new UsernameNotFoundException(email);
		}
		return new UserSpringSecurity(aluno.getId(), aluno.getEmail(), aluno.getSenha(), aluno.getPerfis());
	}

}

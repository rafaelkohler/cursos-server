package br.com.curso.bonsai.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.VideoTecnica;
import br.com.curso.bonsai.entity.dto.VideoTecnicaDTO;
import br.com.curso.bonsai.repositories.VideoTecnicaRepository;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class VideoTecnicaService {

	@Autowired
	private VideoTecnicaRepository repo;
	@Autowired
	private PaginaService paginaService;
	

	public VideoTecnica find(Integer id) throws ObjectNotFoundException {
		Optional<VideoTecnica> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + VideoTecnica.class.getName()));
	}

	@Transactional
	public VideoTecnica insert(VideoTecnica obj) {
		obj.setId(null);
		obj.setPagina(paginaService.find(obj.getPagina().getId()));
		obj = repo.save(obj);
		return obj;
	}
	
	public VideoTecnica update(VideoTecnica obj) {
		VideoTecnica newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}
	
	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}
	
	public List<VideoTecnica> findAll() {
		return repo.findAll();
	}
	
	public Page<VideoTecnica> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public VideoTecnica fromDTO(VideoTecnicaDTO objDto) {
		return new VideoTecnica(objDto.getId(), objDto.getPathVideo(), null);
	}
	
	private void updateDate(VideoTecnica newObj, VideoTecnica obj) {
		newObj.setPathVideo(obj.getPathVideo());
	}

}

package br.com.curso.bonsai.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.Curso;
import br.com.curso.bonsai.entity.conteudocurso.Tecnica;
import br.com.curso.bonsai.entity.dto.TecnicaDTO;
import br.com.curso.bonsai.entity.dto.TecnicaSearchDTO;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.repositories.TecnicaRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class TecnicaService {

	@Autowired
	private TecnicaRepository repo;
	@Autowired
	private CapituloService capituloService;

	public Tecnica find(Integer id) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if (user == null || !user.hasHole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado.");
		}

		Optional<Tecnica> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Tecnica.class.getName()));
	}

	public List<Tecnica> findByCapitulo(Integer idCapitulo) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if (user == null || !user.hasHole(Perfil.ADMIN)) {
			throw new AuthorizationException("Acesso negado.");
		}

		return repo.findByCapitulo(idCapitulo);
	}

//	public Page<Tecnica> search(String nome, Integer page, Integer linesPerPage, String orderBy, String direction){
//		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
//		return repo.searchByCurso(nome, pageRequest);
//	}

	public Page<TecnicaSearchDTO> search(String nome, Integer page, Integer linesPerPage, String orderBy,
			String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), (String) orderBy);

		List<Object[]> list = repo.searchByCurso(nome, pageRequest);
		List<TecnicaSearchDTO> listDTO = new ArrayList<>();
		for (Object o[] : list) {
			Tecnica t = new Tecnica();
			Curso c = new Curso();
			TecnicaSearchDTO dto = new TecnicaSearchDTO();
			t.setId((Integer) o[0]);
			t.setDescricao((String) o[1]);
			t.setTitulo((String) o[2]);
			t.setPathImagem((String) o[3]);
			c.setId((Integer) o[4]);
			c.setDescricao((String) o[5]);
			c.setNome((String) o[6]);
			dto.setCurso(c);
			dto.setTecnica(t);
			listDTO.add(dto);
		}
		Page<TecnicaSearchDTO> resultPage = new PageImpl<>(listDTO);
		return resultPage;
	}

	@Transactional
	public Tecnica insert(Tecnica obj) {
		obj.setId(null);
		obj.setCapitulo(capituloService.find(obj.getCapitulo().getId()));
		obj = repo.save(obj);
		return obj;
	}

	public Tecnica update(Tecnica obj) {
		Tecnica newObj = find(obj.getId());
		updateDate(newObj, obj);
		return repo.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}

	public List<Tecnica> findAll() {
		return repo.findAll();
	}

	public Page<Tecnica> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}

	public Tecnica fromDTO(TecnicaDTO objDto) {
		return new Tecnica(objDto.getId(), objDto.getTitulo(), objDto.getDescricao(), null,
				objDto.getObjetivoTecnica(), objDto.getPathImagem());
	}
	
	public TecnicaDTO fromEntityToDTO(Tecnica obj) {
		return new TecnicaDTO(obj);
	}

	private void updateDate(Tecnica newObj, Tecnica obj) {
		newObj.setTitulo(obj.getTitulo());
		newObj.setDescricao(obj.getDescricao());
		newObj.setObjetivoTecnica(obj.getObjetivoTecnica());
	}

}

package br.com.curso.bonsai.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.services.email.EmailService;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class AuthService {
	
	@Autowired
	private AlunoRepository alunoRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private EmailService emailService;
	
	private Random rand = new Random();
	
	public void sendNewPassword(String email) {
		Aluno aluno = alunoRepository.findByEmail(email);
		if(aluno == null) {
			throw new ObjectNotFoundException("Email não encontrado.");
		}
		String newPass = newPassword();
		aluno.setSenha(bCryptPasswordEncoder.encode(newPass));
		
		alunoRepository.save(aluno);
		emailService.sendNewPasswordEmail(aluno, newPass);
	}

	private String newPassword() {
		char[] vet = new char[10];
		for (int i=0; i<10; i++) {
			vet[i] = randomChar();
		}
		return new String(vet);
	}

	private char randomChar() {
		int opt = rand.nextInt(3);
		if(opt == 0 ) { //gera um dígito
			return (char) (rand.nextInt(10) + 48);
		} else if(opt == 1) { //gera letra maiúscula
			return (char) (rand.nextInt(26) + 65);
		} else { //gera letra minúscula
			return (char) (rand.nextInt(26) + 97);
		}
	}

}

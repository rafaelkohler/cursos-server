package br.com.curso.bonsai.services.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.Categoria;
import br.com.curso.bonsai.entity.Cidade;
import br.com.curso.bonsai.entity.Curso;
import br.com.curso.bonsai.entity.Endereco;
import br.com.curso.bonsai.entity.Estado;
import br.com.curso.bonsai.entity.ItemPedido;
import br.com.curso.bonsai.entity.Pagamento;
import br.com.curso.bonsai.entity.PagamentoComCartao;
import br.com.curso.bonsai.entity.PagamentoComDeposito;
import br.com.curso.bonsai.entity.Pedido;
import br.com.curso.bonsai.entity.builders.CursoBuilder;
import br.com.curso.bonsai.entity.conteudocurso.Capitulo;
import br.com.curso.bonsai.entity.conteudocurso.ImagemTecnica;
import br.com.curso.bonsai.entity.conteudocurso.Pagina;
import br.com.curso.bonsai.entity.conteudocurso.Tecnica;
import br.com.curso.bonsai.entity.enums.EstadoPagamento;
import br.com.curso.bonsai.entity.enums.ObjetivoTecnica;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.entity.enums.TipoAluno;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.repositories.CapituloRepository;
import br.com.curso.bonsai.repositories.CategoriaRepository;
import br.com.curso.bonsai.repositories.CidadeRepository;
import br.com.curso.bonsai.repositories.CursoRepository;
import br.com.curso.bonsai.repositories.EnderecoRepository;
import br.com.curso.bonsai.repositories.EstadoRepository;
import br.com.curso.bonsai.repositories.PaginaRepository;
import br.com.curso.bonsai.repositories.ImagemTecnicaRepository;
import br.com.curso.bonsai.repositories.ItemPedidoRepository;
import br.com.curso.bonsai.repositories.PagamentoRepository;
import br.com.curso.bonsai.repositories.PedidoRepository;
import br.com.curso.bonsai.repositories.TecnicaRepository;

@Service
public class DBService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	@Autowired
	private CursoRepository cursoRepository;
	@Autowired
	private EstadoRepository estadoRepository;
	@Autowired
	private CidadeRepository cidadeRepository;
	@Autowired
	private AlunoRepository alunoRepository;
	@Autowired
	private EnderecoRepository enderecoRepository;
	@Autowired
	private PedidoRepository pedidoRepository;
	@Autowired
	private PagamentoRepository pagamentoRepository;
	@Autowired
	private ItemPedidoRepository cursoPedidoRepository;
	@Autowired
	private CapituloRepository capituloRepository;
	@Autowired
	private TecnicaRepository tecnicaRepository;
	@Autowired
	private PaginaRepository paginaRepository;
	@Autowired 
	private ImagemTecnicaRepository imagemTecnicaRepository;
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	public void instantiateTestDataBase() throws ParseException {
		Categoria cat1 = new Categoria(null, "Coníferas");
		Categoria cat2 = new Categoria(null, "Caducifólias");
		Categoria cat3 = new Categoria(null, "Florífera");
		Categoria cat4 = new Categoria(null, "Frutífera");
		Categoria cat5 = new Categoria(null, "Perenifólias");
		
		Curso c1 = new CursoBuilder(null, "Pinheiro Negro")
				.descricao("Caracteristica e manejo da espécie")
				.preco(110.00)
				.categoria(cat1)
				.build();
		Curso c2 = new CursoBuilder(null, "Acer Palmatum")
				.descricao("Desenvolvimento e técnicas da espécie")
				.preco(99.00)
				.categoria(cat2)
				.build();
		Curso c3 = new CursoBuilder(null, "Shimpaku Kishu")
				.descricao("Manutenção e manejo da espécie")
				.preco(100.00)
				.categoria(cat1)
				.build();
		
		cat1.getCursos().addAll(Arrays.asList(c1, c3));
		cat2.getCursos().addAll(Arrays.asList(c2));

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2, cat3, cat4, cat5));
		cursoRepository.saveAll(Arrays.asList(c1, c2, c3));
		
		Estado est1 = new Estado(null, "Paraná");
		Estado est2 = new Estado(null, "São Paulo");

		Cidade cid1 = new Cidade(null, "Curitiba", est1);
		Cidade cid2 = new Cidade(null, "São Paulo", est2);
		Cidade cid3 = new Cidade(null, "Campinas", est2);

		est1.getCidades().addAll(Arrays.asList(cid1));
		est2.getCidades().addAll(Arrays.asList(cid2, cid3));
		
		estadoRepository.saveAll(Arrays.asList(est1, est2));
		cidadeRepository.saveAll(Arrays.asList(cid1, cid2, cid3));
		
		Aluno aluno1 = new Aluno(null, "Rafael Kohler", "rafael.kohler@gmail.com", "03969833914", TipoAluno.PESSOA_FISICA, encoder.encode("123"));
		Aluno aluno2 = new Aluno(null, "Maria Lucia ", "rafaelkohlerdev@gmail.com", "03969833914", TipoAluno.PESSOA_FISICA, encoder.encode("123"));

		aluno1.addPerfil(Perfil.ADMIN);
		
		aluno1.getTelefones().addAll(Arrays.asList("999449784"));
		aluno2.getTelefones().addAll(Arrays.asList("998813506"));
		
		Endereco e1 = new Endereco(null, "Alameda Dom Pedro", "95", "562", "Batel", "80420060", aluno1, cid1);
		Endereco e2 = new Endereco(null, "Padre Anchieta", "307", "926", "Bigorrilho", "80730000", aluno1, cid2);
		
		aluno1.getEnderecos().addAll(Arrays.asList(e1, e2));
		
		alunoRepository.saveAll(Arrays.asList(aluno1, aluno2));
		enderecoRepository.saveAll(Arrays.asList(e1, e2));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Pedido ped1 = new Pedido(null, sdf.parse("30/09/2020 10:32"), aluno1);
		Pedido ped2 = new Pedido(null, sdf.parse("17/11/2020 15:45"), aluno1);
		
		Pagamento pgto1 = new PagamentoComCartao(null, EstadoPagamento.QUITADO, ped1, 6);
		Pagamento pgto2 = new PagamentoComDeposito(null, EstadoPagamento.PENDENTE, ped2, sdf.parse("20/10/2020 00:00"));
		
		ped1.setPagamento(pgto1);
		ped2.setPagamento(pgto2);
		
		aluno1.getPedidos().addAll(Arrays.asList(ped1));
		aluno2.getPedidos().addAll(Arrays.asList(ped2));
		
		pedidoRepository.saveAll(Arrays.asList(ped1, ped2));
		pagamentoRepository.saveAll(Arrays.asList(pgto1, pgto2));
		
		ItemPedido cp1 = new ItemPedido(ped1, c1, 0.0, 1, c1.getPreco());
		ItemPedido cp2 = new ItemPedido(ped1, c2, 0.0, 1, c2.getPreco());
		ItemPedido cp3 = new ItemPedido(ped2, c3, 0.0, 1, c3.getPreco());
		
		ped1.getCursos().addAll(Arrays.asList(cp1, cp2));
		ped2.getCursos().addAll(Arrays.asList(cp3));
		
		c1.getItensPedido().addAll(Arrays.asList(cp1));
		c2.getItensPedido().addAll(Arrays.asList(cp2));
		c3.getItensPedido().addAll(Arrays.asList(cp3));
		
		
		Capitulo cap1 = new Capitulo(null, "Desenvolvimento", "Crescimento", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTRHnE84wh29D3bHP3WagJ08o07aQj3-y2AHA&usqp=CAU", c1);
		Capitulo cap2 = new Capitulo(null, "Estrutura", "Estrutura Primária", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdRDBS6NgRP0u1UZXZuXsXaukPgl9A9w_tcQ&usqp=CAU", c1);
		Capitulo cap3 = new Capitulo(null, "Ramificacao", "Ramificacao secundaria", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPd2blAZxVeBb8DFBEaaOYcwtvu_gdMrXsmQ&usqp=CAU", c1);
		
		c1.getCapitulos().addAll(Arrays.asList(cap1, cap2, cap3));
		cursoPedidoRepository.saveAll(Arrays.asList(cp1, cp2, cp3));
		capituloRepository.saveAll(Arrays.asList(cap1, cap2, cap3));
		
		Tecnica tec1 = new Tecnica(null, "Crescimento", "Descricao técnica 1", cap1, ObjetivoTecnica.CRESCIMENTO, "");
		Tecnica tec2 = new Tecnica(null, "Crescimento", "Descricao técnica 2", cap1, ObjetivoTecnica.CRESCIMENTO, "");
		Tecnica tec3 = new Tecnica(null, "Crescimento", "Descricao técnica 2", cap1, ObjetivoTecnica.CRESCIMENTO, "");
		Tecnica tec4 = new Tecnica(null, "Estrutura", "Descricao técnica 4", cap2, ObjetivoTecnica.ESTRUTURA_PRIMARIA, "");
		Tecnica tec5 = new Tecnica(null, "Estrutura", "Descricao técnica 5", cap2, ObjetivoTecnica.ESTRUTURA_PRIMARIA, "");
		Tecnica tec6 = new Tecnica(null, "Ramificacao", "Descricao técnica 6", cap3, ObjetivoTecnica.RAMIFICACAO, "");
		
		cap1.getTecnicas().addAll(Arrays.asList(tec1, tec2, tec3));
		cap2.getTecnicas().addAll(Arrays.asList(tec4, tec5));
		cap3.getTecnicas().addAll(Arrays.asList(tec6));
		tecnicaRepository.saveAll(Arrays.asList(tec1, tec2, tec3, tec4, tec5, tec6));
		
		Pagina p1 = new Pagina(null, "Texto para descrever a etapa1 da técnica1", 1, tec1);
		Pagina p2 = new Pagina(null, "Texto para descrever a etapa2 da técnica1", 2, tec1);
		Pagina p3 = new Pagina(null, "Texto para descrever a etapa3 da técnica1", 3, tec1);
		Pagina p4 = new Pagina(null, "Texto para descrever a etapa4 da técnica1", 4, tec1);
		Pagina p5 = new Pagina(null, "Texto para descrever a etapa1 da técnica2", 1, tec2);
		Pagina p6 = new Pagina(null, "Texto para descrever a etapa2 da técnica2", 2, tec2);
		Pagina p7 = new Pagina(null, "Texto para descrever a etapa1 da técnica3", 1, tec3);
		Pagina p8 = new Pagina(null, "Texto para descrever a etapa1 da técnica4", 1, tec4);
		Pagina p9 = new Pagina(null, "Texto para descrever a etapa1 da técnica5", 1, tec5);
		Pagina p10 = new Pagina(null, "Texto para descrever a etapa1 da técnica6", 1, tec6);
		
		tec1.getPaginas().addAll(Arrays.asList(p1, p2, p3, p4));
		tec2.getPaginas().addAll(Arrays.asList(p5, p6));
		tec3.getPaginas().addAll(Arrays.asList(p7));
		tec4.getPaginas().addAll(Arrays.asList(p8));
		tec5.getPaginas().addAll(Arrays.asList(p9));
		tec6.getPaginas().addAll(Arrays.asList(p10));
		paginaRepository.saveAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10));
		
		ImagemTecnica img1 = new ImagemTecnica(null, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTWVd_22GFdt-AhZzaVgiFCKVFLsdn1m4bX6g&usqp=CAU", p1);
		ImagemTecnica img2 = new ImagemTecnica(null, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSV3DLdVGRVXzknXdZdPPnoLG6vfP-1Q5o9QA&usqp=CAU", p1);
		
		p1.getImagens().addAll(Arrays.asList(img1, img2));
		imagemTecnicaRepository.saveAll(Arrays.asList(img1, img2));
	}

}

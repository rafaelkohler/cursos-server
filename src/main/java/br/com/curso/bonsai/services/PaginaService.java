package br.com.curso.bonsai.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.Pagina;
import br.com.curso.bonsai.entity.conteudocurso.Tecnica;
import br.com.curso.bonsai.entity.dto.PaginaDTO;
import br.com.curso.bonsai.entity.dto.PaginaSearchDTO;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.repositories.PaginaRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class PaginaService {

	@Autowired
	private PaginaRepository repo;
	@Autowired
	private TecnicaService tecnicaService;

	public Pagina find(Integer id) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		Optional<Pagina> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pagina.class.getName()));
	}
	
	public List<Pagina> findByTecnica(Integer idTecnica) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN)) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		return repo.findByTecnica(idTecnica);
	}
	
//	public Page<Pagina> search(String nome, Integer page, Integer linesPerPage, String orderBy, String direction){
//		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
//		return repo.searchByTecnica(nome, pageRequest);
//	}
	
	public Page<PaginaSearchDTO> search(String titulo, Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), (String) orderBy);
		
		List<Object[]> list = repo.searchByTecnica(titulo, pageRequest);
		List<PaginaSearchDTO> listDTO = new ArrayList<>();
		for (Object o[] : list) {
			Pagina pagina = new Pagina();
			Tecnica tecnica = new Tecnica();
			PaginaSearchDTO dto = new PaginaSearchDTO();
			pagina.setId((Integer) o[0]);
			pagina.setOrdem((Integer) o[1]);
			pagina.setTexto((String) o[2]);
			tecnica.setTitulo((String) o[3]);
			dto.setTecnica(tecnica);
			dto.setPagina(pagina);
			listDTO.add(dto);
		}
		Page<PaginaSearchDTO> resultPage = new PageImpl<>(listDTO);
		return resultPage;
	}

	@Transactional
	public Pagina insert(Pagina obj) {
		obj.setId(null);
		obj.setTecnica(tecnicaService.find(obj.getTecnica().getId()));
		obj = repo.save(obj);
		return obj;
	}
	
	public Pagina update(Pagina obj) {
		Pagina newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}
	
	public List<Pagina> findAll() {
		return repo.findAll();
	}
	
	public Page<Pagina> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public Pagina fromDTO(PaginaDTO objDto) {
		return new Pagina(objDto.getId(), objDto.getTexto(), objDto.getOrdem(), null);
	}
	
	private void updateDate(Pagina newObj, Pagina obj) {
		newObj.setTexto(obj.getTexto());
		newObj.setOrdem(obj.getOrdem());
	}

}

package br.com.curso.bonsai.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.ItemPedido;
import br.com.curso.bonsai.entity.PagamentoComBoleto;
import br.com.curso.bonsai.entity.Pedido;
import br.com.curso.bonsai.entity.enums.EstadoPagamento;
import br.com.curso.bonsai.repositories.ItemPedidoRepository;
import br.com.curso.bonsai.repositories.PagamentoRepository;
import br.com.curso.bonsai.repositories.PedidoRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.email.EmailService;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class PedidoService {

	@Autowired
	private PedidoRepository repo;
	@Autowired
	private BoletoService boletoService;
	@Autowired
	private PagamentoRepository pagamentoRepository;
	@Autowired
	private CursoService cursoService;
	@Autowired
	private ItemPedidoRepository itemPedidoRepository;
	@Autowired
	private AlunoService alunoService;
	@Autowired
	private EmailService emailService;

	public Pedido find(Integer id) throws ObjectNotFoundException {
		Optional<Pedido> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pedido.class.getName()));
	}
	
	@Transactional
	public Pedido insert(Pedido obj) {
		obj.setId(null);
		obj.setDataPedido(new Date());
		obj.setAluno(alunoService.find(obj.getAluno().getId()));
		obj.getPagamento().setEstadoPagamento(EstadoPagamento.PENDENTE);
		obj.getPagamento().setPedido(obj);
		if(obj.getPagamento() instanceof PagamentoComBoleto) {
			PagamentoComBoleto pagto = (PagamentoComBoleto) obj.getPagamento();
			boletoService.preencherPagamentoComBoleto(pagto, obj.getDataPedido());
		}
		obj = repo.save(obj);
		pagamentoRepository.save(obj.getPagamento());
		for (ItemPedido item : obj.getCursos()) {
			item.setDesconto(0.0);
			item.setCurso(cursoService.find(item.getCurso().getId()));
			item.setPreco(item.getCurso().getPreco());
			item.setPedido(obj);
		}
		itemPedidoRepository.saveAll(obj.getCursos());
		emailService.sendOrderConfirmationHtmlEmail(obj);
		return obj;
	}
	
	public Page<Pedido> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		UserSpringSecurity user = UserService.authenticated();
		if (user == null) {
			throw new AuthorizationException("Acesso negado!");
		}
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		Aluno aluno = alunoService.find(user.getId());
		return repo.findByAluno(aluno, pageRequest);
	}
	
}

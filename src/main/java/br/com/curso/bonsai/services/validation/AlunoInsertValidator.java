package br.com.curso.bonsai.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.dto.AlunoNewDTO;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.resources.exceptions.FieldMessage;


public class AlunoInsertValidator implements ConstraintValidator<AlunoInsert, AlunoNewDTO> {
	
	@Autowired
	private AlunoRepository repo;
	
	@Override
	public void initialize(AlunoInsert ann) {}

	@Override
	public boolean isValid(AlunoNewDTO objDto, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();

//		if(objDto.getTipo().equals(TipoAluno.PESSOA_FISICA.getCod()) && !BR.isValidCPF(objDto.getCpf())) {
//			list.add(new FieldMessage("cpf", "CPF inválido"));
//		}
		
		Aluno aux = repo.findByEmail(objDto.getEmail());
		if(aux != null) {
			list.add(new FieldMessage("email", "Email já existe."));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
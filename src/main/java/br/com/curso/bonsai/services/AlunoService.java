package br.com.curso.bonsai.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.dto.AlunoDTO;
import br.com.curso.bonsai.entity.dto.AlunoNewDTO;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.repositories.EnderecoRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class AlunoService {

	@Autowired
	private AlunoRepository repo;
	@Autowired
	private EnderecoRepository enderecoRepository;
	@Autowired
	private BCryptPasswordEncoder encoder;


	public Aluno find(Integer id) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		Optional<Aluno> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Aluno.class.getName()));
	}

	@Transactional
	public Aluno insert(Aluno obj) {
		obj.setId(null);
		obj = repo.save(obj);
		if(obj.getEnderecos() != null && !obj.getEnderecos().isEmpty()) {
			enderecoRepository.saveAll(obj.getEnderecos());
		}
		return obj;
	}
	
	public Aluno update(Aluno obj) {
		Aluno newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir aluno que possui cursos comprados.");
		}
	}
	
	public List<Aluno> findAll() {
		return repo.findAll();
	}
	
	public Page<Aluno> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public Aluno fromDTO(AlunoDTO objDto) {
		return new Aluno(objDto.getId(), objDto.getNome(), objDto.getEmail());
	}
	
	
	private void updateDate(Aluno newObj, Aluno obj) {
		newObj.setNome(obj.getNome());
		newObj.setEmail(obj.getEmail());
	}
	
//	public Aluno fromDTO(AlunoNewDTO objDto) {
//		Aluno aluno = new Aluno(null, objDto.getNome(), objDto.getEmail(), objDto.getCpf(), TipoAluno.toEnum(objDto.getTipo()), encoder.encode(objDto.getSenha()));
//		Cidade cidade = new Cidade(objDto.getCidadeId(), null, null);
//		Endereco end = new Endereco(null, objDto.getLogradouro(), objDto.getNumero(), objDto.getComplemento(), objDto.getBairro(), objDto.getCep(), aluno, cidade);
//		aluno.getEnderecos().add(end);
//		aluno.getTelefones().add(objDto.getTelefone1());
//		if(objDto.getTelefone2() != null) {
//			aluno.getTelefones().add(objDto.getTelefone2());
//		}
//		if(objDto.getTelefone3() != null) {
//			aluno.getTelefones().add(objDto.getTelefone3());
//		}
//		return aluno;
//	}
	
	public Aluno fromDTO(AlunoNewDTO objDto) {
		Aluno aluno = new Aluno(null, objDto.getNome(), objDto.getEmail(), encoder.encode(objDto.getSenha()));
		aluno.getTelefones().add(objDto.getTelefone1());
		return aluno;
	}

}

package br.com.curso.bonsai.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.ImagemTecnica;
import br.com.curso.bonsai.entity.dto.ImagemTecnicaDTO;
import br.com.curso.bonsai.entity.enums.Perfil;
import br.com.curso.bonsai.repositories.ImagemTecnicaRepository;
import br.com.curso.bonsai.security.UserSpringSecurity;
import br.com.curso.bonsai.services.exceptions.AuthorizationException;
import br.com.curso.bonsai.services.exceptions.DataIntegrityException;
import br.com.curso.bonsai.services.exceptions.ObjectNotFoundException;

@Service
public class ImagemTecnicaService {

	@Autowired
	private ImagemTecnicaRepository repo;
	@Autowired
	private PaginaService paginaService;
	

	public ImagemTecnica find(Integer id) throws ObjectNotFoundException {
		Optional<ImagemTecnica> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + ImagemTecnica.class.getName()));
	}

	@Transactional
	public ImagemTecnica insert(ImagemTecnica obj) {
		obj.setId(null);
		obj.setPagina(paginaService.find(obj.getPagina().getId()));
		obj = repo.save(obj);
		return obj;
	}
	
	public ImagemTecnica update(ImagemTecnica obj) {
		ImagemTecnica newObj = find(obj.getId());
		updateDate(newObj, obj); 
		return repo.save(newObj);
	}
	
	public void delete(Integer id) {
		find(id);
		try {
			repo.deleteById(id);
		} catch(DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir.");
		}
	}
	
	public List<ImagemTecnica> findAll() {
		return repo.findAll();
	}
	
	public Page<ImagemTecnica> findPage(Integer page, Integer linesPerPage, String orderBy, String direction){
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repo.findAll(pageRequest);
	}
	
	public List<ImagemTecnica> findByPagina(Integer idPagina) throws ObjectNotFoundException {
		UserSpringSecurity user = UserService.authenticated();
		if(user == null || !user.hasHole(Perfil.ADMIN)) {
			throw new AuthorizationException("Acesso negado.");
		}
		
		return repo.findByPagina(idPagina);
	}
	
	public ImagemTecnica fromDTO(ImagemTecnicaDTO objDto) {
		return new ImagemTecnica(objDto.getId(), objDto.getPathImagem(), null);
	}
	
	private void updateDate(ImagemTecnica newObj, ImagemTecnica obj) {
		newObj.setPathImagem(obj.getPathImagem());
	}

}

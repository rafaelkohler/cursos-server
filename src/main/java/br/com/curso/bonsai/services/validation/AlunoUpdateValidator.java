package br.com.curso.bonsai.services.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.dto.AlunoDTO;
import br.com.curso.bonsai.repositories.AlunoRepository;
import br.com.curso.bonsai.resources.exceptions.FieldMessage;


public class AlunoUpdateValidator implements ConstraintValidator<AlunoUpdate, AlunoDTO> {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private AlunoRepository repo;
	
	@Override
	public void initialize(AlunoUpdate ann) {}

	@Override
	public boolean isValid(AlunoDTO objDto, ConstraintValidatorContext context) {
		
		@SuppressWarnings("unchecked")
		Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		Integer uriId = Integer.parseInt(map.get("id"));
				
		List<FieldMessage> list = new ArrayList<>();

		Aluno aux = repo.findByEmail(objDto.getEmail());
		if(aux != null && !aux.getId().equals(uriId)) {
			list.add(new FieldMessage("email", "Email já existe."));
		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
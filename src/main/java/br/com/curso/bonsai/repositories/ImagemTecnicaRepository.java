package br.com.curso.bonsai.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.ImagemTecnica;

@Repository
public interface ImagemTecnicaRepository extends JpaRepository<ImagemTecnica, Integer> {
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT img.* FROM ImagemTecnica img WHERE img.pagina_id = :idPagina", nativeQuery = true)
	List<ImagemTecnica> findByPagina(Integer idPagina);

}

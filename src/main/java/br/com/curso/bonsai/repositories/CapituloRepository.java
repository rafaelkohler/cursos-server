package br.com.curso.bonsai.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.Capitulo;

@Repository
public interface CapituloRepository extends JpaRepository<Capitulo, Integer> {
	
	@Transactional(readOnly = true)
	Capitulo findByCurso(String nomeCurso);
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT cap.* FROM Capitulo cap INNER JOIN Curso cur ON cap.curso_id = cur.id WHERE cur.nome LIKE %:nome%", nativeQuery = true )
	Page<Capitulo> search(@Param("nome") String nome, Pageable pageRequest);
	

}

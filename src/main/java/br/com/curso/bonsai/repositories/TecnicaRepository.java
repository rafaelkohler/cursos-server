package br.com.curso.bonsai.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.Tecnica;

@Repository
public interface TecnicaRepository extends JpaRepository<Tecnica, Integer> {

	@Transactional(readOnly = true)
	List<Tecnica> findByObjetivoTecnica(Integer objetivo);

//	@Transactional(readOnly = true)
//	@Query(value = "SELECT DISTINCT tec.* FROM Tecnica tec INNER JOIN Capitulo cap ON tec.capitulo_id = cap.id INNER JOIN Curso cur ON cap.curso_id = cur.id WHERE cur.nome LIKE %:nome%", nativeQuery = true)
//	Page<Tecninca> searchByCurso(@Param("nome") String nome, Pageable pageRequest);
//	
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT tec.id as tecnica_id, tec.descricao as descricao_tecnica, tec.titulo, tec.pathImagem as path_imagem, cur.id, cur.descricao, cur.nome FROM Tecnica tec INNER JOIN Capitulo cap ON tec.capitulo_id = cap.id INNER JOIN Curso cur ON cap.curso_id = cur.id WHERE cur.nome LIKE %:nome%", nativeQuery = true)
	List<Object[]> searchByCurso(@Param("nome") String nome, Pageable pageRequest);
	
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT tec.* FROM Tecnica tec WHERE tec.capitulo_id = :idCapitulo", nativeQuery = true)
	List<Tecnica> findByCapitulo(Integer idCapitulo);

}

package br.com.curso.bonsai.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Integer> {

	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT obj.* FROM Curso obj INNER JOIN Categoria cat ON obj.id = cat.id WHERE obj.nome LIKE %:nome% AND cat.id = :categoria", nativeQuery = true )
	Page<Curso> search(@Param("nome") String nome, @Param("categoria") Integer cat, Pageable pageRequest);
}

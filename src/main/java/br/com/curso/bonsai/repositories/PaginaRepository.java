package br.com.curso.bonsai.repositories;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.curso.bonsai.entity.conteudocurso.Pagina;

@Repository
public interface PaginaRepository extends JpaRepository<Pagina, Integer> {

//	@Transactional(readOnly = true)
//	@Query(value = "SELECT DISTINCT pag.* FROM Pagina pag ", nativeQuery = true)
//	Page<Pagina> searchByTecnica(@Param("nome") String nome, Pageable pageRequest);
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT pag.id as pagina_id, pag.ordem, pag.texto, tec.titulo FROM Pagina pag INNER JOIN Tecnica tec ON pag.tecnica_id = tec.id WHERE tec.titulo LIKE %:titulo%", nativeQuery = true)
	List<Object[]> searchByTecnica(@Param("titulo") String titulo, Pageable pageRequest);
	
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT DISTINCT pag.* FROM Pagina pag WHERE pag.tecnica_id = :idTecnica", nativeQuery = true)
	List<Pagina> findByTecnica(Integer idTecnica);
	
}

package br.com.curso.bonsai.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.curso.bonsai.entity.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {

}

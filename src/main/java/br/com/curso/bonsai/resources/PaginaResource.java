package br.com.curso.bonsai.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.curso.bonsai.entity.conteudocurso.Pagina;
import br.com.curso.bonsai.entity.dto.PaginaDTO;
import br.com.curso.bonsai.entity.dto.PaginaSearchDTO;
import br.com.curso.bonsai.entity.dto.PaginaTecnicaDTO;
import br.com.curso.bonsai.resources.utils.URL;
import br.com.curso.bonsai.services.PaginaService;

@RestController
@RequestMapping(value = "/paginas")
public class PaginaResource {

	@Autowired
	private PaginaService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Pagina> find(@PathVariable Integer id) {
		Pagina obj = service.find(id);

		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value = "/tecninca/{idTecnica}", method = RequestMethod.GET)
	public ResponseEntity<List<Pagina>> findByTecnica(@PathVariable Integer idTecnica) {
		List<Pagina> obj = service.findByTecnica(idTecnica);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Pagina obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody PaginaDTO objDto, @PathVariable Integer id) {
		Pagina obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Pagina> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page<PaginaDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "titulo") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Pagina> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<PaginaDTO> listDto = list.map(capitulo -> new PaginaDTO(capitulo));
		return ResponseEntity.ok().body(listDto);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<Object>> search(@RequestParam(value = "titulo", defaultValue = "") String titulo,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "titulo") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {

		String nomeDecode = URL.decodeParam(titulo);
		Page<PaginaSearchDTO> list = service.search(nomeDecode, page, linesPerPage, orderBy, direction);
		Page<Object> listDTO = list.map(obj -> new PaginaTecnicaDTO(obj));
		return ResponseEntity.ok().body(listDTO);
	}
}

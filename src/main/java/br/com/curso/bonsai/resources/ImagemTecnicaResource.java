package br.com.curso.bonsai.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.curso.bonsai.entity.conteudocurso.ImagemTecnica;
import br.com.curso.bonsai.entity.dto.ImagemTecnicaDTO;
import br.com.curso.bonsai.services.ImagemTecnicaService;

@RestController
@RequestMapping(value = "/imagenstecnica")
public class ImagemTecnicaResource {

	@Autowired
	private ImagemTecnicaService service;

	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
	public ResponseEntity<ImagemTecnica> find(@PathVariable Integer id) {
		ImagemTecnica obj = service.find(id);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody ImagemTecnica obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody ImagemTecnicaDTO objDto, @PathVariable Integer id) {
		ImagemTecnica obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<ImagemTecnica> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ImagemTecnicaDTO>> findAll() {
		List<ImagemTecnica> list = service.findAll();
		List<ImagemTecnicaDTO> listDto = list.stream().map(cat -> new ImagemTecnicaDTO(cat)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/page", method=RequestMethod.GET)
	public ResponseEntity<Page<ImagemTecnicaDTO>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<ImagemTecnica> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<ImagemTecnicaDTO> listDto = list.map(cat -> new ImagemTecnicaDTO(cat));
		return ResponseEntity.ok().body(listDto);
	}
	

	@RequestMapping(value = "/pagina/{idPagina}", method = RequestMethod.GET)
	public ResponseEntity<List<ImagemTecnica>> findByPagina(@PathVariable Integer idPagina) {
		List<ImagemTecnica> obj = service.findByPagina(idPagina);

		return ResponseEntity.ok().body(obj);
	}

}

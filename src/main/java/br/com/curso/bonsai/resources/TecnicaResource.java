package br.com.curso.bonsai.resources;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.curso.bonsai.entity.conteudocurso.Tecnica;
import br.com.curso.bonsai.entity.dto.TecnicaCursoDTO;
import br.com.curso.bonsai.entity.dto.TecnicaDTO;
import br.com.curso.bonsai.entity.dto.TecnicaSearchDTO;
import br.com.curso.bonsai.resources.utils.URL;
import br.com.curso.bonsai.services.TecnicaService;

@RestController
@RequestMapping(value = "/tecnicas")
public class TecnicaResource {

	@Autowired
	private TecnicaService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<TecnicaDTO> find(@PathVariable Integer id) {
		Tecnica obj = service.find(id);
		TecnicaDTO objDTO = service.fromEntityToDTO(obj);
		return ResponseEntity.ok().body(objDTO);
	}

	@RequestMapping(value = "/capitulo/{idCapitulo}", method = RequestMethod.GET)
	public ResponseEntity<List<Tecnica>> findByCapitulo(@PathVariable Integer idCapitulo) {
		List<Tecnica> obj = service.findByCapitulo(idCapitulo);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Tecnica obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody TecnicaDTO objDto, @PathVariable Integer id) {
		Tecnica obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Tecnica> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public ResponseEntity<Page<TecnicaDTO>> findPage(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "titulo") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {
		Page<Tecnica> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<TecnicaDTO> listDto = list.map(capitulo -> new TecnicaDTO(capitulo));
		return ResponseEntity.ok().body(listDto);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<Object>> search(@RequestParam(value = "nome", defaultValue = "") String nome,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
			@RequestParam(value = "orderBy", defaultValue = "titulo") String orderBy,
			@RequestParam(value = "direction", defaultValue = "ASC") String direction) {

		String nomeDecode = URL.decodeParam(nome);
		Page<TecnicaSearchDTO> list = service.search(nomeDecode, page, linesPerPage, orderBy, direction);
		Page<Object> listDTO = list.map(obj -> new TecnicaCursoDTO(obj));
		return ResponseEntity.ok().body(listDTO);
	}
}

package br.com.curso.bonsai.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.curso.bonsai.entity.conteudocurso.VideoTecnica;
import br.com.curso.bonsai.entity.dto.VideoTecnicaDTO;
import br.com.curso.bonsai.services.VideoTecnicaService;

@RestController
@RequestMapping(value = "/videostecnica")
public class VideoTecnicaResource {

	@Autowired
	private VideoTecnicaService service;

	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
	public ResponseEntity<VideoTecnica> find(@PathVariable Integer id) {
		VideoTecnica obj = service.find(id);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody VideoTecnica obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody VideoTecnicaDTO objDto, @PathVariable Integer id) {
		VideoTecnica obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<VideoTecnica> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<VideoTecnicaDTO>> findAll() {
		List<VideoTecnica> list = service.findAll();
		List<VideoTecnicaDTO> listDto = list.stream().map(cat -> new VideoTecnicaDTO(cat)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/page", method=RequestMethod.GET)
	public ResponseEntity<Page<VideoTecnicaDTO>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<VideoTecnica> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<VideoTecnicaDTO> listDto = list.map(cat -> new VideoTecnicaDTO(cat));
		return ResponseEntity.ok().body(listDto);
	}
	

}

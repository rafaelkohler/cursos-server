package br.com.curso.bonsai.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.curso.bonsai.entity.conteudocurso.Capitulo;
import br.com.curso.bonsai.entity.dto.CapituloDTO;
import br.com.curso.bonsai.resources.utils.URL;
import br.com.curso.bonsai.services.CapituloService;

@RestController
@RequestMapping(value = "/capitulos")
public class CapituloResource {

	@Autowired
	private CapituloService service;

	@RequestMapping(value = "/find/{id}", method=RequestMethod.GET)
	public ResponseEntity<CapituloDTO> find(@PathVariable Integer id) {
		Capitulo obj = service.find(id);
		CapituloDTO objDTO = service.fromEntityToDTO(obj);
		return ResponseEntity.ok().body(objDTO);
	}
	
	@RequestMapping(value = "/{nomeCurso}", method=RequestMethod.GET)
	public ResponseEntity<Capitulo> findByNome(@PathVariable String nomeCurso) {
		Capitulo obj = service.findByNomeCurso(nomeCurso);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody Capitulo obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody CapituloDTO objDto, @PathVariable Integer id) {
		Capitulo obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Capitulo> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value="/page", method=RequestMethod.GET)
	public ResponseEntity<Page<CapituloDTO>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="titulo") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<Capitulo> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<CapituloDTO> listDto = list.map(capitulo -> new CapituloDTO(capitulo));
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Page<CapituloDTO>> search(
			@RequestParam(value="nome", defaultValue="") String nome, 
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="titulo") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {

		String nomeDecode = URL.decodeParam(nome);
		Page<Capitulo> list = service.search(nomeDecode, page, linesPerPage, orderBy, direction);
		Page<CapituloDTO> listDTO = list.map(obj -> new CapituloDTO(obj));
		return ResponseEntity.ok().body(listDTO);
	}

}

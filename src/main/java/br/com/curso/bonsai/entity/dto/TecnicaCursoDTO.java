package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

public class TecnicaCursoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String titulo;
	private String descricao;
	private String pathImagem;
	
	private String nomeCurso;
	
	public TecnicaCursoDTO() {}
	
	public TecnicaCursoDTO(TecnicaSearchDTO dto) {
		this.id = dto.getTecnica().getId();
		this.titulo = dto.getTecnica().getTitulo();
		this.descricao = dto.getTecnica().getDescricao();
		this.pathImagem = dto.getTecnica().getPathImagem();
		this.nomeCurso = dto.getCurso().getNome();
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String nome) {
		this.titulo = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}
	
}

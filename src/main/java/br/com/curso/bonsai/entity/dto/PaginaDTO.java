package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.Pagina;

public class PaginaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String texto;
	private Integer ordem;
	
	public PaginaDTO() {}
	
	public PaginaDTO(Pagina pag) {
		this.id = pag.getId();
		this.texto = pag.getTexto();
		this.ordem = pag.getOrdem();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	
}

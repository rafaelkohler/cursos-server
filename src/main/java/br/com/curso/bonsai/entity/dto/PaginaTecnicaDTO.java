package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

public class PaginaTecnicaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String texto;
	private Integer ordem;
	
	private String tituloTecnica;
	
	public PaginaTecnicaDTO() {}
	
	public PaginaTecnicaDTO(PaginaSearchDTO dto) {
		this.id = dto.getPagina().getId();
		this.texto = dto.getPagina().getTexto();
		this.ordem = dto.getPagina().getOrdem();
		this.tituloTecnica = dto.getTecnica().getTitulo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getTituloTecnica() {
		return tituloTecnica;
	}

	public void setTituloTecnica(String tituloTecnica) {
		this.tituloTecnica = tituloTecnica;
	}


	
}

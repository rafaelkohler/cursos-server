package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.VideoTecnica;

public class VideoTecnicaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String pathVideo;
	
	public VideoTecnicaDTO() {}
	
	public VideoTecnicaDTO(VideoTecnica obj) {
		this.id = obj.getId();
		this.pathVideo = obj.getPathVideo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPathVideo() {
		return pathVideo;
	}

	public void setPathVideo(String pathVideo) {
		this.pathVideo = pathVideo;
	}


	
}

package br.com.curso.bonsai.entity.builders;

import br.com.curso.bonsai.entity.Endereco;

public class EnderecoBuilder {
	
	private Integer id;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cep;
	
	public EnderecoBuilder(Integer id, String logradouro) {
		this.id = id;
		this.logradouro = logradouro;
	}
	
	public EnderecoBuilder numero(String num) {
		this.numero = num;
		return this;
	}
	
	public EnderecoBuilder complemento(String complemento) {
		this.complemento = complemento;
		return this;
	}
	
	public EnderecoBuilder bairro(String bairro) {
		this.bairro = bairro;
		return this;
	}
	
	public EnderecoBuilder cep(String cep) {
		this.cep = cep;
		return this;
	}
	
	public Endereco build() {
		Endereco end = new Endereco(this.id, this.logradouro);
		end.setId(this.id);
		end.setLogradouro(this.logradouro);
		end.setNumero(this.numero);
		end.setComplemento(this.complemento);
		end.setBairro(this.bairro);
		end.setCep(this.cep);
		return end;
	}

}

package br.com.curso.bonsai.entity.enums;

public enum ObjetivoTecnica {
	
	CRESCIMENTO(1, "Crescimento"), 
	ESTRUTURA_PRIMARIA(2, "Estrutura primárias"),
	ESTRUTURA_SECUNDARIA(3, "Estrutura secundária"),
	RAMIFICACAO(4, "Ramificação"),
	MANUTENCAO(5, "Manutenção");

	private int cod;
	private String descricao;

	private ObjetivoTecnica(int cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	public int getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static ObjetivoTecnica toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		for (ObjetivoTecnica tc : ObjetivoTecnica.values()) {
			if (cod.equals(tc.getCod())) {
				return tc;
			}
		}
		throw new IllegalArgumentException("Id inválido: " + cod);
	}

}

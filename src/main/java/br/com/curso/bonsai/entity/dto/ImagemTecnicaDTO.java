package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.ImagemTecnica;

public class ImagemTecnicaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String pathImagem;
	
	public ImagemTecnicaDTO() {}
	
	public ImagemTecnicaDTO(ImagemTecnica obj) {
		this.id = obj.getId();
		this.pathImagem = obj.getPathImagem();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}

	
}

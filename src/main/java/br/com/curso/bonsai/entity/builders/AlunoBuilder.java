package br.com.curso.bonsai.entity.builders;

import br.com.curso.bonsai.entity.Aluno;
import br.com.curso.bonsai.entity.enums.TipoAluno;

public class AlunoBuilder {
	
	private Integer id;
	private String nome;
	private String email;
	private String cpf;
	private Integer tipo;
	
	public AlunoBuilder(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public AlunoBuilder email(String email) {
		this.email = email;
		return this;
	}
	
	public AlunoBuilder cpf(String cpf) {
		this.cpf = cpf;
		return this;
	}
	
	public AlunoBuilder tipo(TipoAluno tipo) {
		this.tipo = tipo.getCod();
		return this;
	}
	
	public Aluno build() {
		Aluno aluno = new Aluno(this.id, this.nome);
		aluno.setId(this.id);
		aluno.setNome(this.nome);
		aluno.setEmail(this.email);
		aluno.setCpf(this.cpf);
		aluno.setTipo(TipoAluno.toEnum(this.tipo));
		return aluno;
	}

}

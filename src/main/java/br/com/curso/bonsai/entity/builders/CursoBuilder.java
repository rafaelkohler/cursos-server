package br.com.curso.bonsai.entity.builders;

import br.com.curso.bonsai.entity.Categoria;
import br.com.curso.bonsai.entity.Curso;

public class CursoBuilder  {

	private Integer id;
	private String nome;
	private String descricao;
	private Double preco;
	private Categoria categoria;
	
	
	public CursoBuilder(Integer id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	public CursoBuilder descricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
	
	public CursoBuilder preco(Double preco) {
		this.preco = preco;
		return this;
	}
	
	public CursoBuilder categoria(Categoria categoria) {
		this.categoria = categoria;
		return this;
	}
	
	public Curso build() {
		Curso curso = new Curso(this.id, this.nome);
		curso.setId(this.id);
		curso.setNome(this.nome);
		curso.setDescricao(this.descricao);
		curso.setPreco(this.preco);
		curso.setCategoria(this.categoria);
		return curso;
	}

}

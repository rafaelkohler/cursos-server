package br.com.curso.bonsai.entity;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeName;

import br.com.curso.bonsai.entity.enums.EstadoPagamento;

@Entity
@JsonTypeName("pagamentoComDeposito")
public class PagamentoComDeposito extends Pagamento {
	private static final long serialVersionUID = 1L;
	
	@JsonFormat(pattern="dd/MM/yyyy")
	private Date dataPagamento;
	
	public PagamentoComDeposito() {}

	public PagamentoComDeposito(Integer id, EstadoPagamento estadoPagamento, Pedido pedido, Date dataPagamento) {
		super(id, estadoPagamento, pedido);
		this.dataPagamento = dataPagamento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	

}

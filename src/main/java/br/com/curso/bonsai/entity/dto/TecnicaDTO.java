package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.Tecnica;
import br.com.curso.bonsai.entity.enums.ObjetivoTecnica;
import br.com.curso.bonsai.entity.enums.TipoAluno;

public class TecnicaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String titulo;
	private String descricao;
	private String pathImagem;
	
//	private String nomeCurso;
	
	private Integer objetivoTecnica;
	
	public TecnicaDTO() {}
	
	public TecnicaDTO(Tecnica tecnica) {
		this.id = tecnica.getId();
		this.titulo = tecnica.getTitulo();
		this.descricao = tecnica.getDescricao();
		this.pathImagem = tecnica.getPathImagem();
		this.objetivoTecnica = tecnica.getObjetivoTecnica().getCod();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String nome) {
		this.titulo = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

//	public String getNomeCurso() {
//		if(nomeCurso != null) {
//			return nomeCurso;
//		}
//		return "";
//	}
//
//	public void setNomeCurso(String nomeCurso) {
//		this.nomeCurso = nomeCurso;
//	}

//	public Integer getObjetivoTecnica() {
//		return objetivoTecnica;
//	}

//	public void setObjetivoTecnica(Integer objetivoTecnica) {
//		this.objetivoTecnica = objetivoTecnica;
//	}
	
	public ObjetivoTecnica getObjetivoTecnica() {
		return ObjetivoTecnica.toEnum(objetivoTecnica);
	}

	public void setObjetivoTecnica(ObjetivoTecnica objetivoTecnica) {
		this.objetivoTecnica = objetivoTecnica.getCod();
	}

	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}

}

package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.Curso;
import br.com.curso.bonsai.entity.conteudocurso.Tecnica;

public class TecnicaSearchDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Curso curso;
	private Tecnica tecnica;
	
	public TecnicaSearchDTO() {}
	
	public TecnicaSearchDTO(Curso curso, Tecnica tecnica) {
		this.curso = curso;
		this.tecnica = tecnica;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Tecnica getTecnica() {
		return tecnica;
	}

	public void setTecnica(Tecnica tecnica) {
		this.tecnica = tecnica;
	}

	
}

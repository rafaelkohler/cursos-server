package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.Capitulo;

public class CapituloDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String titulo;
	private String descricao;
	private String pathImagem;
	
	public CapituloDTO() {}
	
	public CapituloDTO(Capitulo capitulo) {
		this.id = capitulo.getId();
		this.titulo = capitulo.getTitulo();
		this.descricao = capitulo.getDescricao();
		this.pathImagem = capitulo.getPathImagem();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String nome) {
		this.titulo = nome;
	}

	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

package br.com.curso.bonsai.entity.conteudocurso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Pagina implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length=2500)
	private String texto;
	private Integer ordem;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="tecnica_id")
	private Tecnica tecnica;
	
	@JsonIgnore
	@OneToMany(mappedBy="pagina")
	private List<ImagemTecnica> imagens = new ArrayList<>();
	
	@JsonIgnore
	@OneToMany(mappedBy="pagina")
	private List<VideoTecnica> videos = new ArrayList<>();
	
	public Pagina() {}

	public Pagina(Integer id, String texto, Integer ordem, Tecnica tecnica) {
		super();
		this.id = id;
		this.texto = texto;
		this.ordem = ordem;
		this.tecnica = tecnica;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public Tecnica getTecnica() {
		return tecnica;
	}

	public void setTecnica(Tecnica tecnica) {
		this.tecnica = tecnica;
	}

	public List<ImagemTecnica> getImagens() {
		return imagens;
	}

	public void setImagens(List<ImagemTecnica> imagens) {
		this.imagens = imagens;
	}
	
	public List<VideoTecnica> getVideos() {
		return videos;
	}

	public void setVideos(List<VideoTecnica> videos) {
		this.videos = videos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagina other = (Pagina) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

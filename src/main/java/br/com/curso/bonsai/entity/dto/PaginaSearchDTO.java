package br.com.curso.bonsai.entity.dto;

import java.io.Serializable;

import br.com.curso.bonsai.entity.conteudocurso.Pagina;
import br.com.curso.bonsai.entity.conteudocurso.Tecnica;

public class PaginaSearchDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Pagina pagina;
	private Tecnica tecnica;
	
	public PaginaSearchDTO() {}
	
	public PaginaSearchDTO(Pagina pagina, Tecnica tecnica) {
		this.pagina = pagina;
		this.tecnica = tecnica;
	}

	public Pagina getPagina() {
		return pagina;
	}

	public void setPagina(Pagina pagina) {
		this.pagina = pagina;
	}

	public Tecnica getTecnica() {
		return tecnica;
	}

	public void setTecnica(Tecnica tecnica) {
		this.tecnica = tecnica;
	}

	
}

package br.com.curso.bonsai.entity.conteudocurso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.curso.bonsai.entity.enums.ObjetivoTecnica;

@Entity
public class Tecnica implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String titulo;
	
	@Column(length=510)
	private String descricao;
	private Integer objetivoTecnica;
	private String pathImagem;
	
	@ManyToOne
	@JoinColumn(name="capitulo_id")
	private Capitulo capitulo;
	
	@JsonIgnore
	@OneToMany(mappedBy="tecnica")
	private List<Pagina> paginas = new ArrayList<>();
	
	public Tecnica() {}

	public Tecnica(Integer id, String titulo, String descricao, Capitulo capitulo, ObjetivoTecnica objetivo, String pathImagem) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.capitulo = capitulo;
		this.objetivoTecnica = (objetivo==null) ? null : objetivo.getCod();
		this.pathImagem = pathImagem;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Capitulo getCapitulo() {
		return capitulo;
	}

	public void setCapitulo(Capitulo capitulo) {
		this.capitulo = capitulo;
	}

	public List<Pagina> getPaginas() {
		return paginas;
	}

	public void setPaginas(List<Pagina> paginas) {
		this.paginas = paginas;
	}
	
	public ObjetivoTecnica getObjetivoTecnica() {
		return ObjetivoTecnica.toEnum(objetivoTecnica);
	}

	public void setObjetivoTecnica(ObjetivoTecnica objetivo) {
		this.objetivoTecnica = objetivo.getCod();
	}
	
	public String getPathImagem() {
		return pathImagem;
	}

	public void setPathImagem(String pathImagem) {
		this.pathImagem = pathImagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tecnica other = (Tecnica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}

package br.com.curso.bonsai.entity.enums;

public enum TipoAluno {
	
	PESSOA_FISICA(1, "Pessoa Física"), PESSOA_JURIDICA(2, "Pessoa Jurídica");

	private int cod;
	private String descricao;

	private TipoAluno(int cod, String descricao) {
		this.cod = cod;
		this.descricao = descricao;
	}

	public int getCod() {
		return cod;
	}

	public String getDescricao() {
		return descricao;
	}

	public static TipoAluno toEnum(Integer cod) {
		if (cod == null) {
			return null;
		}
		for (TipoAluno tc : TipoAluno.values()) {
			if (cod.equals(tc.getCod())) {
				return tc;
			}
		}
		throw new IllegalArgumentException("Id inválido: " + cod);
	}

}

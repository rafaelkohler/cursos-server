package br.com.curso.bonsai;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursobonsaiApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CursobonsaiApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
	}

}
